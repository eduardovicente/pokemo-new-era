function onMove(player, fromPosition, toPosition)
	if (not player:isInFly()) then
		return true
	end

	Game.createFlyTile(toPosition)
	Game.removeFlyTile(fromPosition)
	return true
end
