function onLogout(player)
	local playerId = player:getId()
	if nextUseStaminaTime[playerId] ~= nil then
		nextUseStaminaTime[playerId] = nil
	end

    local currentPokeball = player:getCurrentPokeball()
    if (currentPokeball) then
        local pokeball = Pokeball.new()
        pokeball:loadFromItem(currentPokeball)
        pokeball:back(player)
    end

	return true
end
