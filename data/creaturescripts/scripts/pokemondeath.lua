function onDeath(pokemon, corpse, killer, mostDamageKiller, unjustified, mostDamageUnjustified)
    return pokemon:onPokemonDeath()
end