-- Core API functions implemented in Lua
dofile('data/lib/core/core.lua')
dofile('data/lib/core/combatInfo.lua')

-- Compatibility library for our old Lua API
dofile('data/lib/compat/compat.lua')

-- Abilitys library for order
dofile('data/lib/abilitys/Fly.lua')
dofile('data/lib/Pokeball.lua')