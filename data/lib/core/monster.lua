function Monster.loadFromPokeball(self, pokeball)
    self:setLevel(pokeball.level)
    self:setExperience(pokeball.experience)

    self:setMaxHealth(pokeball.maxHealth)
    --[[local currentHealth = -(self:getHealth() - 1)
    self:addHealth(currentHealth)
    self:addHealth(pokeball.health - 1) ]]
    
    self:setExtraStatus(0, pokeball.extraStatus.hp)
    self:setExtraStatus(1, pokeball.extraStatus.attack)
    self:setExtraStatus(2, pokeball.extraStatus.defense)
    self:setExtraStatus(3, pokeball.extraStatus.specialAttack)
    self:setExtraStatus(4, pokeball.extraStatus.specialDefense)
    self:setExtraStatus(5, pokeball.extraStatus.speed)
end

function Monster.onPokemonDeath(self)
    local trainer = self:getMaster()
    if (not trainer) then
        return true
    end

    local pokeballItem = trainer:getCurrentPokeball()
    if (not pokeballItem) then
        return true
    end

    local pokeball = Pokeball.new()
    pokeball.item = pokeballItem
    pokeball.pokemon = self

    pokeball:onDeath(trainer)
    
    return false
end

function Monster.getExtraStatusTable(self)
    local status = {
        hp = self:getExtraStatus(0),
        attack = self:getExtraStatus(1),
        defense = self:getExtraStatus(2),
        specialAttack = self:getExtraStatus(3),
        specialDefense = self:getExtraStatus(4),
        speed = self:getExtraStatus(5),
    }
    
    return status
end

function Monster.statusToString(self)
    local str = ""
    str = str.. "Name: ".. self:getName().. ", Level: ".. self:getLevel()
    str = str.. ", Attributes {"
    str = str.. " Hp: ".. self:getBaseStatus(0)
    str = str.. " / Attack: ".. self:getBaseStatus(1)
    str = str.. " / Defense: ".. self:getBaseStatus(2)
    str = str.. " / Spc.Attack: ".. self:getBaseStatus(3)
    str = str.. " / Spc.Defense: ".. self:getBaseStatus(4)
    str = str.. " / Speed: ".. self:getBaseStatus(5)
    str = str.. " }"

    return str
end
