FlySystem = {
	Messages = {
		["noPoke"] = "You need a pokemon to fly.",
		["isFlying"] = "You already is flying.",
		["noAbility"] = "Your pokemon cant fly.",
		["noFlying"] = "You are not flying.",
		["cantUp"] = "You cant up there.",
		["cantDown"] = "You cant down there.",
	},

	On = function(self, player)
		if (not self:PokemonCanFly(player)) then
			return player:sendTextMessage(22, self.Messages["noAbility"])
		end

		if (player:isInFly()) then
			return
		end
		
		return player:setIsInFly(true);
	end,

	Off = function(self, player)
		if (not player:isInFly()) then
			return
		end

		local positionTile = Tile(player:getPosition())
		if (positionTile and self:IsFlyTile(positionTile)) then
			return player:sendTextMessage(22, "You cant stop fly here.")
		end
		
		return player:setIsInFly(false);
	end,

	Up = function(self, player)
		if (not player:isInFly()) then
			return player:sendTextMessage(22, self.Messages["noFlying"])
		end

		local fromPosition = player:getPosition()
		local toPosition = player:getPosition()
		toPosition.z = toPosition.z - 1

		local toPositionTile = Tile(toPosition)

		if (toPositionTile and (not self:IsFlyTile(toPositionTile) or not self:CanWalkInTile(toPositionTile))) then
			return player:sendTextMessage(22, self.Messages["cantUp"])
		end

		Game.createFlyTile(toPosition)
		Game.removeFlyTile(fromPosition)
		player:teleportTo(toPosition)
	end,

	Down = function(self, player)
		if (not player:isInFly()) then
			return player:sendTextMessage(22, self.Messages["noFlying"])
		end

		local fromPosition = player:getPosition()
		local toPosition = player:getPosition()
		toPosition.z = toPosition.z + 1

		local fromPositionTile = Tile(fromPosition)
		local toPositionTile = Tile(toPosition)

		if (fromPositionTile and toPositionTile and (not self:IsFlyTile(fromPositionTile) or not self:CanWalkInTile(toPositionTile))) then
			return player:sendTextMessage(22, self.Messages["cantDown"])
		end

		Game.createFlyTile(toPosition)
		Game.removeFlyTile(fromPosition)
		player:teleportTo(toPosition)
	end,

	PokemonCanFly = function(self, player)		
		if (#player:getSummons() <= 0) then
			return player:sendTextMessage(22, self.Messages["noPoke"])
		end

		local pokemon = player:getSummons()[1]
		if (not pokemon:hasAbility(ABILITY_FLY)) then
			return player:sendTextMessage(22, self.Messages["noAbility"])
		end

		return true
	end,

	CanWalkInTile = function(self, tile)
		if (tile:getHouse() or
			tile:hasProperty(CONST_PROP_BLOCKSOLID) or
			tile:hasProperty(TILESTATE_FLOORCHANGE) or
			tile:hasProperty(TILESTATE_BLOCKSOLID) or
			tile:hasProperty(TILESTATE_PROTECTIONZONE) or
			tile:hasProperty(CONST_PROP_BLOCKPROJECTILE)) then		
			return false
		end

		return true
	end,

	IsFlyTile = function(self, tile)
		if (tile:getGround() and tile:getGround():getId() ~= 460) then
			return false
		end

		return true
	end,
}