Pokeball = {}
Pokeball.__index = Pokeball

function Pokeball.new()
    local self = {}
    self.name = "New"
    self.level = 0
    self.experience = 0

    self.health = 150
    self.maxHealth =  150
    self.extraStatus = {
        hp = 1,
        attack = 1,
        defense = 1,
        specialAttack = 1,
        specialDefense = 1,
        speed = 1,
    }

    self.item = nil
    self.pokemon = nil

    return setmetatable(self, Pokeball)
end

function Pokeball.saveInItem(self)
    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONNAME, self.name)
    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONLEVEL, self.level)
    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONEXPERIENCE, self.experience)

    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONHEALTH, self.health)
    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONMAXHEALTH, self.maxHealth)

    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTEHP, self.extraStatus.hp)
    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTEATTACK, self.extraStatus.attack)
    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTEDEFENSE, self.extraStatus.defense)
    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTESPECIALATTACK, self.extraStatus.specialAttack)
    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTESPECIALDEFENSE, self.extraStatus.specialDefense)
    self.item:setAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTESPEED, self.extraStatus.speed)
end

function Pokeball.loadFromItem(self, item)
    if not item:hasAttribute(ITEM_ATTRIBUTE_POKEMONNAME) then
        return false
    end

    self.name = item:getAttribute(ITEM_ATTRIBUTE_POKEMONNAME)
    self.level = item:getAttribute(ITEM_ATTRIBUTE_POKEMONLEVEL)
    self.experience = item:getAttribute(ITEM_ATTRIBUTE_POKEMONEXPERIENCE)

    self.health = item:getAttribute(ITEM_ATTRIBUTE_POKEMONHEALTH)
    self.maxHealth = item:getAttribute(ITEM_ATTRIBUTE_POKEMONMAXHEALTH)

    self.extraStatus = {}
    self.extraStatus.hp = item:getAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTEHP)
    self.extraStatus.attack = item:getAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTEATTACK)
    self.extraStatus.defense = item:getAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTEDEFENSE)
    self.extraStatus.specialAttack = item:getAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTESPECIALATTACK)
    self.extraStatus.specialDefense = item:getAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTESPECIALDEFENSE)
    self.extraStatus.speed = item:getAttribute(ITEM_ATTRIBUTE_POKEMONATTRIBUTESPEED)

    self.item = item
    return setmetatable(self, Pokeball)
end

function Pokeball.loadFromPokemon(self, pokemon)
    if (not pokemon:isCreature() or not pokemon:isMonster()) then
        return false
    end

    self.pokemon = pokemon
    self.name = pokemon:getName()
    self.level = pokemon:getLevel()
    self.experience = pokemon:getExperience()

    self.health = pokemon:getHealth()
    self.maxHealth =  pokemon:getMaxHealth()
    self.extraStatus = pokemon:getExtraStatusTable()
    return true
end

function Pokeball.catch(self, trainer, pokemon)
    -- Save pokemon infos in pokeball
    if (not self:loadFromPokemon(pokemon)) then
        return false
    end

    if (pokemon:getMaster()) then
        trainer:sendCancelMessage("Sorry, you cant catch this pokemon!")
        return false
    end

    -- Save necessary infos in variables
    local trainerPosition = trainer:getPosition()
    local pokemonStatus = pokemon:statusToString()
    local currentHealthChance = (pokemon:getHealth() / pokemon:getMaxHealth()) * 100
    local mathChance = math.random(1, 100)

    if (mathChance >= currentHealthChance) then
        self.item = Item(doPlayerAddItem(trainer, 2650, 1))
        if not self.item then
            return trainer:sendCancelMessage("[Erro] = Cant add a new pokeball!")
        end

        self.health = self.maxHealth
        self:saveInItem()

        trainer:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You catch an ".. self.name)
        trainer:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Infos: ".. pokemonStatus)
        trainerPosition:sendMagicEffect(CONST_ME_FIREWORK_YELLOW)

        pokemon:remove()
    else
        trainer:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Sorry your pokeball broken.")
        trainerPosition:sendMagicEffect(CONST_ME_POFF)
    end

    return true
end

function Pokeball.sendPokemon(self, trainer)
    local position = trainer:getPosition()
    local pokemon = Game.createMonster(self.name, self.health, position)
    if not pokemon then
        trainer:sendCancelMessage("Error: unable to load pokemon (".. self.name.. ") contact the admin.")
        print("Error: unable to load pokemon (".. self.name.. ")")
        return false
    end

    -- Load pokeball infos to pokemon
    pokemon:loadFromPokeball(self)
    self.pokemon = pokemon

    -- Register events in pokemon 
    pokemon:registerEvent("PokemonDeath")

    -- Send effects and convert pokemon
    pokemon:setMaster(trainer)
    trainer:say("Go ".. self.name.. "!", TALKTYPE_SAY)
    position:sendMagicEffect(CONST_ME_MAGIC_RED)
    return true
end

function Pokeball.removePokemon(self)
    -- Save pokemon infos in pokeball
    if (not self:loadFromPokemon(self.pokemon)) then
        return false
    end

    -- Get Trainer
    local trainer = self.pokemon:getMaster()

    -- Send effects and remove pokemon
    self.pokemon:getPosition():sendMagicEffect(CONST_ME_POFF)
    self.pokemon:remove()
    trainer:say("Back ".. self.name.. "!", TALKTYPE_SAY)
    return true
end

function Pokeball.go(self, trainer)
    if (self.item:getActionId() == 5000) then
        trainer:sendCancelMessage("You must equip this pokeball.")
        return false
    end

    local currentPokeball = trainer:getCurrentPokeball()
    if currentPokeball ~= nil then
        local currentPb = Pokeball.new()
        currentPb.item = currentPokeball

        if not currentPb:back(trainer) then
            return false
        end
    end

    -- Summon pokemon
    self:sendPokemon(trainer)

    -- Save current pokeball in player
    trainer:setCurrentPokeball(self.item)

    -- Change portrait and set unmoveable
    self.item:transform(2651, 1)
    self.item:setMoveblePokeball(false)
    return true    
end

function Pokeball.back(self, trainer)
    local currentPokeball = trainer:getCurrentPokeball()
    if (self.health <= 0) then
        trainer:sendCancelMessage("Sorry, This pokemon is fainted.")
        return false
    end

    if currentPokeball ~= self.item then
        trainer:sendCancelMessage("Sorry, this isnot the correct pokeball.")
        return false
    end

    local pokemon = trainer:getSummons()[1]
    if (not pokemon) then
        return false
    end
    
    -- Remove pokemon, and save infos in item
    self.pokemon = pokemon
    self:removePokemon()
    self:saveInItem()

    -- Save current pokeball in player
    trainer:setCurrentPokeball(nil)

    -- Change portrait and set moveable
    self.item:transform(2650, 1)
    self.item:setMoveblePokeball(true)
    return true
end

function Pokeball.onDeath(self, trainer)
    self:removePokemon()
    self.health = 0
    self:saveInItem()

    -- Save current pokeball in player
    trainer:setCurrentPokeball(nil)
    trainer:sendTextMessage(MESSAGE_STATUS_CONSOLE_RED, "Sorry your pokemon is fainted.")

    self.item:setMoveblePokeball(true)
end

function Pokeball.toString(self)
    local str = ""
    str = str.. "Name: ".. self.name.. ", Level: ".. self.level
    str = str.. ", Exp: ".. self.experience
    str = str.. ", Health: ".. self.health.. ", Max Health: ".. self.maxHealth
    str = str.. ", Extra Status {"
    str = str.. " Hp: ".. self.extraStatus.hp
    str = str.. " / Attack: ".. self.extraStatus.attack
    str = str.. " / Defense: ".. self.extraStatus.defense
    str = str.. " / Spc.Attack: ".. self.extraStatus.specialAttack
    str = str.. " / Spc.Defense: ".. self.extraStatus.specialDefense
    str = str.. " / Speed: ".. self.extraStatus.speed
    str = str.. " }"
 
    return str
end
