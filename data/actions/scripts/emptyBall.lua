function onUse(player, item, fromPosition, target, toPosition, isHotkey)
    if not (Pokeball.new():catch(player, target)) then
        return true
    end

    item:remove(1)
    return true
end
