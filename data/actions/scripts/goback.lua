function onUse(player, item, fromPosition, toPosition, isHotkey)
    local pokeball = Pokeball.new()
    pokeball:loadFromItem(item)

    if (item:getId() == 2650) then
        pokeball:go(player)
    elseif (item:getId() == 2651) then
        pokeball:back(player)
    end

    return true
end
