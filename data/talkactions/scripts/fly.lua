function onSay(player, words, param)
	if (param == "on") then
		FlySystem:On(player)
	end

	if (param == "off") then
		FlySystem:Off(player)
	end

	if (param == "up") then
		FlySystem:Up(player)
    	return true
    end

	if (param == "down") then
		FlySystem:Down(player)
    	return true
	end

	return false
end