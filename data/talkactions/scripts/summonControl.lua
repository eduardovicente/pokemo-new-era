function onSay(player, word, param)
	if (param == "on") then
		if (#player:getSummons() <= 0) then
			return
		end
		
		local poke = player:getSummons()[1]
		poke:setControlled(true)
	elseif (param == "off") then
		if (#player:getSummons() <= 0) then
			return
		end
		
		local poke = player:getSummons()[1]
		poke:setControlled(false)
	end

	if (param == "up") then
		if (#player:getSummons() <= 0) then
			return
		end
		
		local poke = player:getSummons()[1]
		Game.playerMovePokemon(poke, DIRECTION_NORTH)
	elseif (param == "down") then
		if (#player:getSummons() <= 0) then
			return
		end
		
		local poke = player:getSummons()[1]
		Game.playerMovePokemon(poke, DIRECTION_SOUTH)
	elseif (param == "left") then
		if (#player:getSummons() <= 0) then
			return
		end
		
		local poke = player:getSummons()[1]
		Game.playerMovePokemon(poke, DIRECTION_EAST)
	elseif (param == "right") then
		if (#player:getSummons() <= 0) then
			return
		end
		
		local poke = player:getSummons()[1]
		Game.playerMovePokemon(poke, DIRECTION_WEST)
	elseif (param == "downRight") then
		if (#player:getSummons() <= 0) then
			return
		end
		
		local poke = player:getSummons()[1]
		Game.playerMovePokemon(poke, DIRECTION_SOUTHWEST)
	elseif (param == "downLeft") then
		if (#player:getSummons() <= 0) then
			return
		end
		
		local poke = player:getSummons()[1]
		Game.playerMovePokemon(poke, DIRECTION_SOUTHEAST)
	elseif (param == "upRight") then
		if (#player:getSummons() <= 0) then
			return
		end
		
		local poke = player:getSummons()[1]
		Game.playerMovePokemon(poke, DIRECTION_NORTHWEST)
	elseif (param == "upLeft") then
		if (#player:getSummons() <= 0) then
			return
		end
		
		local poke = player:getSummons()[1]
		Game.playerMovePokemon(poke, DIRECTION_NORTHEAST)
	end
end