function Monster:onDeath()
	local player = self:getMaster()
	player:sendTextMessage(MESSAGE_INFO_DESCR, "Your pokemon fainted!")
	return true
end

function Monster:onGainExperience(source, exp, rawExp)
	if not source or source:isPlayer() then
		return exp
	end
	
	-- Apply experience stage multiplier
	exp = exp * Game.getExperienceStage(self:getLevel())
	return exp
end