local combat = Combat()
combat:setParameter(COMBAT_PARAM_TYPE, COMBAT_NORMALDAMAGE)
combat:setParameter(COMBAT_PARAM_SPELLTYPE, COMBATSPELL_ATTACK)
combat:setParameter(COMBAT_PARAM_EFFECT, CONST_ME_BLOCKHIT)
combat:setArea(createCombatArea(AREA_CIRCLE3X3))

function onMonsterCombat(monster, attack, defense, specialattack, specialdefense)
    local chance = math.random(1, 5)

    local min = (attack * 1.5)/ chance
    local max = (attack * 2) / chance
    return -min, -max
end

combat:setCallback(CALLBACK_PARAM_MONSTER, "onMonsterCombat")

function onCastSpell(creature, variant)
	return combat:execute(creature, variant)
end
