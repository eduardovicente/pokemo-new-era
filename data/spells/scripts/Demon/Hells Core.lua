local combat = Combat()
combat:setParameter(COMBAT_PARAM_TYPE, COMBAT_FIREDAMAGE)
combat:setParameter(COMBAT_PARAM_SPELLTYPE, COMBATSPELL_SPECIALATTACK)
combat:setParameter(COMBAT_PARAM_EFFECT, CONST_ME_FIREAREA)
combat:setArea(createCombatArea(AREA_CROSS5X5))

function onMonsterCombat(monster, attack, defense, specialattack, specialdefense)
    local chance = math.random(1, 5)

    local min = (attack * 1.5)/ chance
    local max = (attack * 2) / chance
    return -min, -max
end


combat:setCallback(CALLBACK_PARAM_MONSTER, "onMonsterCombat")

function onCastSpell(creature, variant)
	return combat:execute(creature, variant)
end
