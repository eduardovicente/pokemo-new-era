/**
 * The Forgotten Server - a free and open-source MMORPG server emulator
 * Copyright (C) 2016  Mark Samman <mark.samman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "otpch.h"

#include "monsters.h"
#include "monster.h"
#include "spells.h"
#include "combat.h"
#include "weapons.h"
#include "configmanager.h"
#include "game.h"

#include "pugicast.h"

extern Game g_game;
extern Spells* g_spells;
extern Monsters g_monsters;
extern ConfigManager g_config;

spellBlock_t::~spellBlock_t()
{
	if (combatSpell) {
		delete spell;
	}
}

MonsterType::MonsterType()
{
	reset();
}

void MonsterType::reset()
{

	spawnLevel.min = 1;
	spawnLevel.max = 100;

	baseExperience = 0;

	hiddenHealth = false;

	canPushItems = false;
	canPushCreatures = false;
	staticAttackChance = 95;
	maxSummons = 0;
	targetDistance = 1;
	runAwayHealth = 0;
	pushable = true;
	baseSpeed = 200;

	outfit.reset();
	lookcorpse = 0;

	skull = SKULL_NONE;

	conditionImmunities = 0;
	damageImmunities = 0;
	isSummonable = false;
	isIllusionable = false;
	isConvinceable = false;
	isAttackable = true;
	isHostile = true;

	lightLevel = 0;
	lightColor = 0;

	summons.clear();
	lootItems.clear();
	elementMap.clear();

	attackSpells.clear();
	passiveSpells.clear();

	yellSpeedTicks = 0;
	yellChance = 0;
	voiceVector.clear();

	changeTargetSpeed = 0;
	changeTargetChance = 0;

	scriptInterface = nullptr;
	creatureAppearEvent = -1;
	creatureDisappearEvent = -1;
	creatureMoveEvent = -1;
	creatureSayEvent = -1;
	thinkEvent = -1;

	scripts.clear();
}

uint32_t Monsters::getLootRandom()
{
	return uniform_random(0, MAX_LOOTCHANCE) / g_config.getNumber(ConfigManager::RATE_LOOT);
}

void MonsterType::createLoot(Container* corpse)
{
	if (g_config.getNumber(ConfigManager::RATE_LOOT) == 0) {
		corpse->startDecaying();
		return;
	}

	Player* owner = g_game.getPlayerByID(corpse->getCorpseOwner());
	if (!owner || owner->getStaminaMinutes() > 840) {
		for (auto it = lootItems.rbegin(), end = lootItems.rend(); it != end; ++it) {
			auto itemList = createLootItem(*it);
			if (itemList.empty()) {
				continue;
			}

			for (Item* item : itemList) {
				//check containers
				if (Container* container = item->getContainer()) {
					if (!createLootContainer(container, *it)) {
						delete container;
						continue;
					}
				}

				if (g_game.internalAddItem(corpse, item) != RETURNVALUE_NOERROR) {
					corpse->internalAddThing(item);
				}
			}
		}

		if (owner) {
			std::ostringstream ss;
			ss << "Loot of " << nameDescription << ": " << corpse->getContentDescription();

			if (owner->getParty()) {
				owner->getParty()->broadcastPartyLoot(ss.str());
			} else {
				owner->sendTextMessage(MESSAGE_LOOT, ss.str());
			}
		}
	} else {
		std::ostringstream ss;
		ss << "Loot of " << nameDescription << ": nothing (due to low stamina)";

		if (owner->getParty()) {
			owner->getParty()->broadcastPartyLoot(ss.str());
		} else {
			owner->sendTextMessage(MESSAGE_LOOT, ss.str());
		}
	}

	corpse->startDecaying();
}

std::vector<Item*> MonsterType::createLootItem(const LootBlock& lootBlock)
{
	int32_t itemCount = 0;

	uint32_t randvalue = Monsters::getLootRandom();
	if (randvalue < lootBlock.chance) {
		if (Item::items[lootBlock.id].stackable) {
			itemCount = randvalue % lootBlock.countmax + 1;
		} else {
			itemCount = 1;
		}
	}

	std::vector<Item*> itemList;
	while (itemCount > 0) {
		uint16_t n = static_cast<uint16_t>(std::min<int32_t>(itemCount, 100));
		Item* tmpItem = Item::CreateItem(lootBlock.id, n);
		if (!tmpItem) {
			break;
		}

		itemCount -= n;

		if (lootBlock.subType != -1) {
			tmpItem->setSubType(lootBlock.subType);
		}

		if (lootBlock.actionId != -1) {
			tmpItem->setActionId(lootBlock.actionId);
		}

		if (!lootBlock.text.empty()) {
			tmpItem->setText(lootBlock.text);
		}

		itemList.push_back(tmpItem);
	}
	return itemList;
}

bool MonsterType::createLootContainer(Container* parent, const LootBlock& lootblock)
{
	auto it = lootblock.childLoot.begin(), end = lootblock.childLoot.end();
	if (it == end) {
		return true;
	}

	for (; it != end && parent->size() < parent->capacity(); ++it) {
		auto itemList = createLootItem(*it);
		for (Item* tmpItem : itemList) {
			if (Container* container = tmpItem->getContainer()) {
				if (!createLootContainer(container, *it)) {
					delete container;
				} else {
					parent->internalAddThing(container);
				}
			} else {
				parent->internalAddThing(tmpItem);
			}
		}
	}
	return !parent->empty();
}

Monsters::Monsters()
{
	loaded = false;
}

bool Monsters::loadFromXml(bool reloading /*= false*/)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file("data/monster/monsters.xml");
	if (!result) {
		printXMLError("Error - Monsters::loadFromXml", "data/monster/monsters.xml", result);
		return false;
	}

	loaded = true;

	std::list<std::pair<MonsterType*, std::string>> monsterScriptList;

	for (auto monsterNode : doc.child("monsters").children()) {
		std::cout << "[Monsters::loadMonster] load : " << monsterNode.attribute("name").as_string() << std::endl;
		loadMonster("data/monster/" + std::string(monsterNode.attribute("file").as_string()), monsterNode.attribute("name").as_string(), monsterScriptList, reloading);
	}

	if (!monsterScriptList.empty()) {
		if (!scriptInterface) {
			scriptInterface.reset(new LuaScriptInterface("Monster Interface"));
			scriptInterface->initState();
		}

		for (const auto& scriptEntry : monsterScriptList) {
			MonsterType* mType = scriptEntry.first;
			if (scriptInterface->loadFile("data/monster/scripts/" + scriptEntry.second) == 0) {
				mType->scriptInterface = scriptInterface.get();
				mType->creatureAppearEvent = scriptInterface->getEvent("onCreatureAppear");
				mType->creatureDisappearEvent = scriptInterface->getEvent("onCreatureDisappear");
				mType->creatureMoveEvent = scriptInterface->getEvent("onCreatureMove");
				mType->creatureSayEvent = scriptInterface->getEvent("onCreatureSay");
				mType->thinkEvent = scriptInterface->getEvent("onThink");
			} else {
				std::cout << "[Warning - Monsters::loadMonster] Can not load script: " << scriptEntry.second << std::endl;
				std::cout << scriptInterface->getLastLuaError() << std::endl;
			}
		}
	}
	return true;
}

bool Monsters::reload()
{
	loaded = false;

	scriptInterface.reset();

	return loadFromXml(true);
}

ConditionDamage* Monsters::getDamageCondition(ConditionType_t conditionType,
        int32_t maxDamage, int32_t minDamage, int32_t startDamage, uint32_t tickInterval)
{
	ConditionDamage* condition = static_cast<ConditionDamage*>(Condition::createCondition(CONDITIONID_COMBAT, conditionType, 0, 0));
	condition->setParam(CONDITION_PARAM_TICKINTERVAL, tickInterval);
	condition->setParam(CONDITION_PARAM_MINVALUE, minDamage);
	condition->setParam(CONDITION_PARAM_MAXVALUE, maxDamage);
	condition->setParam(CONDITION_PARAM_STARTVALUE, startDamage);
	condition->setParam(CONDITION_PARAM_DELAYED, 1);
	return condition;
}

bool Monsters::deserializeSpell(const pugi::xml_node& node, spellBlock_t& sb, const std::string& description)
{
	std::string name;
	std::string scriptName;
	bool isScripted;

	pugi::xml_attribute attr;
	if ((attr = node.attribute("script"))) {
		scriptName = attr.as_string();
		isScripted = true;
	} else if ((attr = node.attribute("name"))) {
		name = attr.as_string();
		isScripted = false;
	} else {
		return false;
	}

	if ((attr = node.attribute("cooldown"))) {
		sb.cooldown = std::max<int32_t>(1, pugi::cast<int32_t>(attr.value()));
	}

	if ((attr = node.attribute("word"))) {
		sb.word = attr.as_string();
	}

	if ((attr = node.attribute("level"))) {
		sb.level = pugi::cast<uint32_t>(attr.value());
	}

	if ((attr = node.attribute("chance"))) {
		uint32_t chance = pugi::cast<uint32_t>(attr.value());
		if (chance > 100) {
			chance = 100;
		}
		sb.chance = chance;
	}

	if ((attr = node.attribute("range"))) {
		uint32_t range = pugi::cast<uint32_t>(attr.value());
		if (range > (Map::maxViewportX * 2)) {
			range = Map::maxViewportX * 2;
		}
		sb.range = range;
	}

	if ((attr = node.attribute("min"))) {
		sb.minCombatValue = pugi::cast<int32_t>(attr.value());
	}

	if ((attr = node.attribute("max"))) {
		sb.maxCombatValue = pugi::cast<int32_t>(attr.value());

		//normalize values
		if (std::abs(sb.minCombatValue) > std::abs(sb.maxCombatValue)) {
			int32_t value = sb.maxCombatValue;
			sb.maxCombatValue = sb.minCombatValue;
			sb.minCombatValue = value;
		}
	}

	if (auto spell = g_spells->getSpellByName(name)) {
		sb.spell = spell;
		return true;
	}


	CombatSpell* combatSpell = nullptr;
	bool needTarget = false;
	bool needDirection = false;

	if (isScripted) {
		if ((attr = node.attribute("direction"))) {
			needDirection = attr.as_bool();
		}

		if ((attr = node.attribute("target"))) {
			needTarget = attr.as_bool();
		}

		std::unique_ptr<CombatSpell> combatSpellPtr(new CombatSpell(nullptr, needTarget, needDirection));
		if (!combatSpellPtr->loadScript("data/" + g_spells->getScriptBaseName() + "/scripts/" + scriptName)) {
			return false;
		}

		if (!combatSpellPtr->loadScriptCombat()) {
			return false;
		}

		combatSpell = combatSpellPtr.release();
		combatSpell->getCombat()->setPlayerCombatValues(COMBAT_FORMULA_DAMAGE, sb.minCombatValue, 0, sb.maxCombatValue, 0);
	} else {

		Combat* combat = new Combat;
		if ((attr = node.attribute("length"))) {
			int32_t length = pugi::cast<int32_t>(attr.value());
			if (length > 0) {
				int32_t spread = 3;

				//need direction spell
				if ((attr = node.attribute("spread"))) {
					spread = std::max<int32_t>(0, pugi::cast<int32_t>(attr.value()));
				}

				AreaCombat* area = new AreaCombat();
				area->setupArea(length, spread);
				combat->setArea(area);

				needDirection = true;
			}
		}

		if ((attr = node.attribute("radius"))) {
			int32_t radius = pugi::cast<int32_t>(attr.value());

			//target spell
			if ((attr = node.attribute("target"))) {
				needTarget = attr.as_bool();
			}

			AreaCombat* area = new AreaCombat();
			area->setupArea(radius);
			combat->setArea(area);
		}

		std::string tmpName = asLowerCaseString(name);

		if (tmpName == "melee") {
			sb.isMelee = true;

			pugi::xml_attribute attackAttribute, skillAttribute;
			if ((attackAttribute = node.attribute("attack")) && (skillAttribute = node.attribute("skill"))) {
				sb.minCombatValue = 0;
				sb.maxCombatValue = -Weapons::getMaxMeleeDamage(pugi::cast<int32_t>(skillAttribute.value()), pugi::cast<int32_t>(attackAttribute.value()));
			}

			sb.range = 1;
			combat->setParam(COMBAT_PARAM_TYPE, COMBAT_NORMALDAMAGE);
			combat->setOrigin(ORIGIN_MELEE);
		} else if (tmpName == "speed") {
			int32_t speedChange = 0;
			int32_t duration = 10000;

			if ((attr = node.attribute("duration"))) {
				duration = pugi::cast<int32_t>(attr.value());
			}

			if ((attr = node.attribute("speedchange"))) {
				speedChange = pugi::cast<int32_t>(attr.value());
				if (speedChange < -1000) {
					//cant be slower than 100%
					speedChange = -1000;
				}
			}

			ConditionType_t conditionType;
			if (speedChange > 0) {
				conditionType = CONDITION_HASTE;
				combat->setParam(COMBAT_PARAM_AGGRESSIVE, 0);
			} else {
				conditionType = CONDITION_PARALYZE;
			}

			ConditionSpeed* condition = static_cast<ConditionSpeed*>(Condition::createCondition(CONDITIONID_COMBAT, conditionType, duration, 0));
			condition->setFormulaVars(speedChange / 1000.0, 0, speedChange / 1000.0, 0);
			combat->setCondition(condition);
		} else {
			std::cout << "[Error - Monsters::deserializeSpell] - " << description << " - Unknown spell name: " << name << std::endl;
			delete combat;
			return false;
		}

		combat->setPlayerCombatValues(COMBAT_FORMULA_DAMAGE, sb.minCombatValue, 0, sb.maxCombatValue, 0);
		combatSpell = new CombatSpell(combat, needTarget, needDirection);

		for (auto attributeNode : node.children()) {
			if ((attr = attributeNode.attribute("key"))) {
				const char* value = attr.value();
				if (strcasecmp(value, "shooteffect") == 0) {
					if ((attr = attributeNode.attribute("value"))) {
						ShootType_t shoot = getShootType(attr.as_string());
						if (shoot != CONST_ANI_NONE) {
							combat->setParam(COMBAT_PARAM_DISTANCEEFFECT, shoot);
						} else {
							std::cout << "[Warning - Monsters::deserializeSpell] " << description << " - Unknown shootEffect: " << attr.as_string() << std::endl;
						}
					}
				} else if (strcasecmp(value, "areaeffect") == 0) {
					if ((attr = attributeNode.attribute("value"))) {
						MagicEffectClasses effect = getMagicEffect(attr.as_string());
						if (effect != CONST_ME_NONE) {
							combat->setParam(COMBAT_PARAM_EFFECT, effect);
						} else {
							std::cout << "[Warning - Monsters::deserializeSpell] " << description << " - Unknown areaEffect: " << attr.as_string() << std::endl;
						}
					}
				} else {
					std::cout << "[Warning - Monsters::deserializeSpells] Effect type \"" << attr.as_string() << "\" does not exist." << std::endl;
				}
			}
		}
	}

	sb.spell = combatSpell;
	if (combatSpell) {
		sb.combatSpell = true;
	}
	return true;
}

bool Monsters::loadMonster(const std::string& file, const std::string& monsterName, std::list<std::pair<MonsterType*, std::string>>& monsterScriptList, bool reloading /*= false*/)
{
	MonsterType* mType = nullptr;
	bool new_mType = true;

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(file.c_str());
	if (!result) {
		printXMLError("Error - Monsters::loadMonster", file, result);
		return false;
	}

	pugi::xml_node monsterNode = doc.child("monster");
	if (!monsterNode) {
		std::cout << "[Error - Monsters::loadMonster] Missing monster node in: " << file << std::endl;
		return false;
	}

	pugi::xml_attribute attr;
	if (!(attr = monsterNode.attribute("name"))) {
		std::cout << "[Error - Monsters::loadMonster] Missing name in: " << file << std::endl;
		return false;
	}

	if (reloading) {
		mType = getMonsterType(monsterName);
		if (mType != nullptr) {
			new_mType = false;
			mType->reset();
		}
	}

	if (new_mType) {
		mType = &monsters[asLowerCaseString(monsterName)];
	}

	mType->name = attr.as_string();

	if ((attr = monsterNode.attribute("nameDescription"))) {
		mType->nameDescription = attr.as_string();
	} else {
		mType->nameDescription = "a " + mType->name;
		toLowerCaseString(mType->nameDescription);
	}

	if ((attr = monsterNode.attribute("experience"))) {
		mType->baseExperience = pugi::cast<uint64_t>(attr.value());
	}

	if ((attr = monsterNode.attribute("speed"))) {
		mType->baseSpeed = pugi::cast<int32_t>(attr.value());
	}

	if ((attr = monsterNode.attribute("skull"))) {
		mType->skull = getSkullType(attr.as_string());
	}

	if ((attr = monsterNode.attribute("script"))) {
		monsterScriptList.emplace_back(mType, attr.as_string());
	}

	pugi::xml_node node;
	if ((node = monsterNode.child("types"))) {
		if ((attr = node.attribute("first"))) {
			mType->types.first = getPokeType(attr);
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing first type. " << file << std::endl;
		}

		if ((attr = node.attribute("second"))) {
			mType->types.first = getPokeType(attr);
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing second type. " << file << std::endl;
		}
	}

	if ((node = monsterNode.child("spawnLevel"))) {
		if ((attr = node.attribute("min"))) {
			mType->spawnLevel.min = pugi::cast<uint32_t>(attr.value());
		}
		else {
			std::cout << "[Error - Monsters::loadMonster] Missing spawn level min. " << file << std::endl;
		}

		if ((attr = node.attribute("max"))) {
			mType->spawnLevel.max = pugi::cast<uint32_t>(attr.value());
		}
		else {
			std::cout << "[Error - Monsters::loadMonster] Missing spawn level max. " << file << std::endl;
		}
	}

	if ((node = monsterNode.child("statusPerLevel"))) {
		if ((attr = node.attribute("hp"))) {
			mType->statusPerLevel.hp = pugi::cast<double>(attr.value());
		}
		else {
			std::cout << "[Error - Monsters::loadMonster] Missing status per level hp. " << file << std::endl;
		}

		if ((attr = node.attribute("attack"))) {
			mType->statusPerLevel.attack = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status per level attack. " << file << std::endl;
		}

		if ((attr = node.attribute("defense"))) {
			mType->statusPerLevel.defense = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status per level defense. " << file << std::endl;
		}

		if ((attr = node.attribute("specialattack"))) {
			mType->statusPerLevel.specialAttack = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status per level special attack. " << file << std::endl;
		}

		if ((attr = node.attribute("specialdefense"))) {
			mType->statusPerLevel.specialDefense = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status per level special defense. " << file << std::endl;
		}

		if ((attr = node.attribute("speed"))) {
			mType->statusPerLevel.speed = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status per level speed. " << file << std::endl;
		}
	}

	if ((node = monsterNode.child("status"))) {
		if ((attr = node.attribute("hp"))) {
			mType->baseStatus.hp = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status health. " << file << std::endl;
		}

		if ((attr = node.attribute("attack"))) {
			mType->baseStatus.attack = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status attack. " << file << std::endl;
		}

		if ((attr = node.attribute("defense"))) {
			mType->baseStatus.defense = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status defense. " << file << std::endl;
		}

		if ((attr = node.attribute("specialattack"))) {
			mType->baseStatus.specialAttack = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status special attack. " << file << std::endl;
		}

		if ((attr = node.attribute("specialdefense"))) {
			mType->baseStatus.specialDefense = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status special defense. " << file << std::endl;
		}

		if ((attr = node.attribute("speed"))) {
			mType->baseStatus.speed = pugi::cast<double>(attr.value());
		} else {
			std::cout << "[Error - Monsters::loadMonster] Missing status speed. " << file << std::endl;
		}
	}

	if ((node = monsterNode.child("flags"))) {
		for (auto flagNode : node.children()) {
			attr = flagNode.first_attribute();
			const char* attrName = attr.name();
			if (strcasecmp(attrName, "summonable") == 0) {
				mType->isSummonable = attr.as_bool();
			} else if (strcasecmp(attrName, "attackable") == 0) {
				mType->isAttackable = attr.as_bool();
			} else if (strcasecmp(attrName, "hostile") == 0) {
				mType->isHostile = attr.as_bool();
			} else if (strcasecmp(attrName, "illusionable") == 0) {
				mType->isIllusionable = attr.as_bool();
			} else if (strcasecmp(attrName, "convinceable") == 0) {
				mType->isConvinceable = attr.as_bool();
			} else if (strcasecmp(attrName, "pushable") == 0) {
				mType->pushable = attr.as_bool();
			} else if (strcasecmp(attrName, "canpushitems") == 0) {
				mType->canPushItems = attr.as_bool();
			} else if (strcasecmp(attrName, "canpushcreatures") == 0) {
				mType->canPushCreatures = attr.as_bool();
			} else if (strcasecmp(attrName, "staticattack") == 0) {
				uint32_t staticAttack = pugi::cast<uint32_t>(attr.value());
				if (staticAttack > 100) {
					std::cout << "[Warning - Monsters::loadMonster] staticattack greater than 100. " << file << std::endl;
					staticAttack = 100;
				}

				mType->staticAttackChance = staticAttack;
			} else if (strcasecmp(attrName, "lightlevel") == 0) {
				mType->lightLevel = pugi::cast<uint16_t>(attr.value());
			} else if (strcasecmp(attrName, "lightcolor") == 0) {
				mType->lightColor = pugi::cast<uint16_t>(attr.value());
			} else if (strcasecmp(attrName, "targetdistance") == 0) {
				mType->targetDistance = std::max<int32_t>(1, pugi::cast<int32_t>(attr.value()));
			} else if (strcasecmp(attrName, "runonhealth") == 0) {
				mType->runAwayHealth = pugi::cast<int32_t>(attr.value());
			} else if (strcasecmp(attrName, "hidehealth") == 0) {
				mType->hiddenHealth = attr.as_bool();
			} else {
				std::cout << "[Warning - Monsters::loadMonster] Unknown flag attribute: " << attrName << ". " << file << std::endl;
			}
		}

		//if a monster can push creatures,
		// it should not be pushable
		if (mType->canPushCreatures && mType->pushable) {
			mType->pushable = false;
		}
	}

	if ((node = monsterNode.child("targetchange"))) {
		if ((attr = node.attribute("speed")) || (attr = node.attribute("interval"))) {
			mType->changeTargetSpeed = pugi::cast<uint32_t>(attr.value());
		} else {
			std::cout << "[Warning - Monsters::loadMonster] Missing targetchange speed. " << file << std::endl;
		}

		if ((attr = node.attribute("chance"))) {
			mType->changeTargetChance = pugi::cast<int32_t>(attr.value());
		} else {
			std::cout << "[Warning - Monsters::loadMonster] Missing targetchange chance. " << file << std::endl;
		}
	}

	if ((node = monsterNode.child("look"))) {
		if ((attr = node.attribute("type"))) {
			mType->outfit.lookType = pugi::cast<uint16_t>(attr.value());
			if ((attr = node.attribute("head"))) {
				mType->outfit.lookHead = pugi::cast<uint16_t>(attr.value());
			}
			if ((attr = node.attribute("body"))) {
				mType->outfit.lookBody = pugi::cast<uint16_t>(attr.value());
			}
			if ((attr = node.attribute("legs"))) {
				mType->outfit.lookLegs = pugi::cast<uint16_t>(attr.value());
			}
			if ((attr = node.attribute("feet"))) {
				mType->outfit.lookFeet = pugi::cast<uint16_t>(attr.value());
			}
			if ((attr = node.attribute("addons"))) {
				mType->outfit.lookAddons = pugi::cast<uint16_t>(attr.value());
			}
		} else if ((attr = node.attribute("typeex"))) {
			mType->outfit.lookTypeEx = pugi::cast<uint16_t>(attr.value());
		} else {
			std::cout << "[Warning - Monsters::loadMonster] Missing look type/typeex. " << file << std::endl;
		}

		if ((attr = node.attribute("mount"))) {
			mType->outfit.lookMount = pugi::cast<uint16_t>(attr.value());
		}
		if ((attr = node.attribute("corpse"))) {
			mType->lookcorpse = pugi::cast<uint16_t>(attr.value());
		}
	}

	if ((node = monsterNode.child("attack"))) {
		if (!deserializeSpell(node, mType->normalAttack, monsterName)) {
			std::cout << "[Warning - Monsters::loadMonster] Cant load attack. " << file << std::endl;
		}
	}

	if ((node = monsterNode.child("spells"))) {
		for (auto attackNode : node.children()) {
			spellBlock_t sb;
			if (deserializeSpell(attackNode, sb, monsterName)) {
				mType->attackSpells.emplace_back(std::move(sb));
			} else {
				std::cout << "[Warning - Monsters::loadMonster] Cant load spells. " << file << std::endl;
			}
		}
	}

	if ((node = monsterNode.child("passives"))) {
		for (auto passiveNode : node.children()) {
			spellBlock_t sb;
			if (deserializeSpell(passiveNode, sb, monsterName)) {
				mType->passiveSpells.emplace_back(std::move(sb));
			} else {
				std::cout << "[Warning - Monsters::loadMonster] Cant load passive spell. " << file << std::endl;
			}
		}
	}

	if ((node = monsterNode.child("abilitys"))) {
		for (auto elementNode : node.children()) {			
			if ((attr = elementNode.attribute("cut"))) {
				mType->abilityMap[ABILITY_CUT] = attr.as_bool();
			} else if ((attr = elementNode.attribute("rocksmash"))) {
				mType->abilityMap[ABILITY_ROCKSMASH] = attr.as_bool();
			} else if ((attr = elementNode.attribute("dig"))) {
				mType->abilityMap[ABILITY_DIG] = attr.as_bool();
			} else if ((attr = elementNode.attribute("light"))) {
				mType->abilityMap[ABILITY_LIGHT] = attr.as_bool();
			} else if ((attr = elementNode.attribute("blink"))) {
				mType->abilityMap[ABILITY_BLINK] = attr.as_bool();
			} else if ((attr = elementNode.attribute("surf"))) {
				mType->abilityMap[ABILITY_SURF] = attr.as_bool();
			} else if ((attr = elementNode.attribute("ride"))) {
				mType->abilityMap[ABILITY_RIDE] = attr.as_bool();
			} else if ((attr = elementNode.attribute("fly"))) {
				mType->abilityMap[ABILITY_FLY] = attr.as_bool();
			} else if ((attr = elementNode.attribute("teleport"))) {
				mType->abilityMap[ABILITY_TELEPORT] = attr.as_bool();
			} else {
				std::cout << "[Warning - Monsters::loadMonster] Unknown ability. " << file << std::endl;
			}
		}
	}

	if ((node = monsterNode.child("immunities"))) {
		for (auto immunityNode : node.children()) {
			if ((attr = immunityNode.attribute("name"))) {
				std::string tmpStrValue = asLowerCaseString(attr.as_string());
				if (tmpStrValue == "bug") {
					mType->damageImmunities |= COMBAT_BUGDAMAGE;
				} else if (tmpStrValue == "dragon") {
					mType->damageImmunities |= COMBAT_DRAGONDAMAGE;
				} else if (tmpStrValue == "fairy") {
					mType->damageImmunities |= COMBAT_DRAGONDAMAGE;
				} else if (tmpStrValue == "fire") {
					mType->damageImmunities |= COMBAT_FIREDAMAGE;
				} else if (tmpStrValue == "ghost") {
					mType->damageImmunities |= COMBAT_GHOSTDAMAGE;
				} else if (tmpStrValue == "ground") {
					mType->damageImmunities |= COMBAT_GROUNDDAMAGE;
				} else if (tmpStrValue == "normal") {
					mType->damageImmunities |= COMBAT_NORMALDAMAGE;
				} else if (tmpStrValue == "psychic") {
					mType->damageImmunities |= COMBAT_PSYCHICDAMAGE;
				} else if (tmpStrValue == "steel") {
					mType->damageImmunities |= COMBAT_STEELDAMAGE;
				} else if (tmpStrValue == "dark") {
					mType->damageImmunities |= COMBAT_DARKDAMAGE;
				} else if (tmpStrValue == "electric") {
					mType->damageImmunities |= COMBAT_ELECTRICDAMAGE;
				} else if (tmpStrValue == "fighting") {
					mType->damageImmunities |= COMBAT_FIGHTINGDAMAGE;
				} else if (tmpStrValue == "flying") {
					mType->damageImmunities |= COMBAT_FLYINGDAMAGE;
				} else if (tmpStrValue == "grass") {
					mType->damageImmunities |= COMBAT_GRASSDAMAGE;
				} else if (tmpStrValue == "ice") {
					mType->damageImmunities |= COMBAT_ICEDAMAGE;
				} else if (tmpStrValue == "poison") {
					mType->damageImmunities |= COMBAT_POISONDAMAGE;
				} else if (tmpStrValue == "rock") {
					mType->damageImmunities |= COMBAT_ROCKDAMAGE;
				} else if (tmpStrValue == "water") {
					mType->damageImmunities |= COMBAT_WATERDAMAGE;
				} else {
					std::cout << "[Warning - Monsters::loadMonster] Unknown immunity name " << attr.as_string() << ". " << file << std::endl;
				}
			}
		}
	}

	if ((node = monsterNode.child("voices"))) {
		if ((attr = node.attribute("speed")) || (attr = node.attribute("interval"))) {
			mType->yellSpeedTicks = pugi::cast<uint32_t>(attr.value());
		} else {
			std::cout << "[Warning - Monsters::loadMonster] Missing voices speed. " << file << std::endl;
		}

		if ((attr = node.attribute("chance"))) {
			mType->yellChance = pugi::cast<uint32_t>(attr.value());
		} else {
			std::cout << "[Warning - Monsters::loadMonster] Missing voices chance. " << file << std::endl;
		}

		for (auto voiceNode : node.children()) {
			voiceBlock_t vb;
			if ((attr = voiceNode.attribute("sentence"))) {
				vb.text = attr.as_string();
			} else {
				std::cout << "[Warning - Monsters::loadMonster] Missing voice sentence. " << file << std::endl;
			}

			if ((attr = voiceNode.attribute("yell"))) {
				vb.yellText = attr.as_bool();
			} else {
				vb.yellText = false;
			}
			mType->voiceVector.emplace_back(vb);
		}
	}

	if ((node = monsterNode.child("loot"))) {
		for (auto lootNode : node.children()) {
			LootBlock lootBlock;
			if (loadLootItem(lootNode, lootBlock)) {
				mType->lootItems.emplace_back(std::move(lootBlock));
			} else {
				std::cout << "[Warning - Monsters::loadMonster] Cant load loot. " << file << std::endl;
			}
		}
	}

	if ((node = monsterNode.child("elements"))) {
		for (auto elementNode : node.children()) {
			if ((attr = elementNode.attribute("bugPercent"))) {
				mType->elementMap[COMBAT_BUGDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("dragonPercent"))) {
				mType->elementMap[COMBAT_DRAGONDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("fairyPercent"))) {
				mType->elementMap[COMBAT_FAIRYDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("firePercent"))) {
				mType->elementMap[COMBAT_FIREDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("ghostPercent"))) {
				mType->elementMap[COMBAT_GHOSTDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("groundPercent"))) {
				mType->elementMap[COMBAT_GROUNDDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("normalPercent"))) {
				mType->elementMap[COMBAT_NORMALDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("psychicPercent"))) {
				mType->elementMap[COMBAT_PSYCHICDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("steelPercent"))) {
				mType->elementMap[COMBAT_STEELDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("darkPercent"))) {
				mType->elementMap[COMBAT_DARKDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("electricPercent"))) {
				mType->elementMap[COMBAT_ELECTRICDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("fightingPercent"))) {
				mType->elementMap[COMBAT_FIGHTINGDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("flyingPercent"))) {
				mType->elementMap[COMBAT_FLYINGDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("grassPercent"))) {
				mType->elementMap[COMBAT_GRASSDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("icePercent"))) {
				mType->elementMap[COMBAT_ICEDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("poisonPercent"))) {
				mType->elementMap[COMBAT_POISONDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("rockPercent"))) {
				mType->elementMap[COMBAT_ROCKDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else if ((attr = elementNode.attribute("waterPercent"))) {
				mType->elementMap[COMBAT_WATERDAMAGE] = pugi::cast<int32_t>(attr.value());
			} else {
				std::cout << "[Warning - Monsters::loadMonster] Unknown element percent. " << file << std::endl;
			}
		}
	}

	if ((node = monsterNode.child("summons"))) {
		if ((attr = node.attribute("maxSummons"))) {
			mType->maxSummons = std::min<uint32_t>(pugi::cast<uint32_t>(attr.value()), 100);
		} else {
			std::cout << "[Warning - Monsters::loadMonster] Missing summons maxSummons. " << file << std::endl;
		}

		for (auto summonNode : node.children()) {
			int32_t chance = 100;
			int32_t speed = 1000;
			bool force = false;

			if ((attr = summonNode.attribute("speed")) || (attr = summonNode.attribute("interval"))) {
				speed = pugi::cast<int32_t>(attr.value());
			}

			if ((attr = summonNode.attribute("chance"))) {
				chance = pugi::cast<int32_t>(attr.value());
			}
			
			if ((attr = summonNode.attribute("force"))) {
				force = attr.as_bool();
			}

			if ((attr = summonNode.attribute("name"))) {
				summonBlock_t sb;
				sb.name = attr.as_string();
				sb.speed = speed;
				sb.chance = chance;
				sb.force = force;
				mType->summons.emplace_back(sb);
			} else {
				std::cout << "[Warning - Monsters::loadMonster] Missing summon name. " << file << std::endl;
			}
		}
	}

	if ((node = monsterNode.child("script"))) {
		for (auto eventNode : node.children()) {
			if ((attr = eventNode.attribute("name"))) {
				mType->scripts.emplace_back(attr.as_string());
			} else {
				std::cout << "[Warning - Monsters::loadMonster] Missing name for script event. " << file << std::endl;
			}
		}
	}

	mType->summons.shrink_to_fit();
	mType->lootItems.shrink_to_fit();
	mType->attackSpells.shrink_to_fit();
	mType->passiveSpells.shrink_to_fit();
	mType->voiceVector.shrink_to_fit();
	mType->scripts.shrink_to_fit();
	return true;
}

bool Monsters::loadLootItem(const pugi::xml_node& node, LootBlock& lootBlock)
{
	pugi::xml_attribute attr;
	if ((attr = node.attribute("id"))) {
		lootBlock.id = pugi::cast<int32_t>(attr.value());
	}

	if (lootBlock.id == 0) {
		return false;
	}

	if ((attr = node.attribute("countmax"))) {
		lootBlock.countmax = std::max<int32_t>(1, pugi::cast<int32_t>(attr.value()));
	} else {
		lootBlock.countmax = 1;
	}

	if ((attr = node.attribute("chance")) || (attr = node.attribute("chance1"))) {
		lootBlock.chance = std::min<int32_t>(MAX_LOOTCHANCE, pugi::cast<int32_t>(attr.value()));
	} else {
		lootBlock.chance = MAX_LOOTCHANCE;
	}

	if (Item::items[lootBlock.id].isContainer()) {
		loadLootContainer(node, lootBlock);
	}

	//optional
	if ((attr = node.attribute("subtype"))) {
		lootBlock.subType = pugi::cast<int32_t>(attr.value());
	} else {
		uint32_t charges = Item::items[lootBlock.id].charges;
		if (charges != 0) {
			lootBlock.subType = charges;
		}
	}

	if ((attr = node.attribute("actionId"))) {
		lootBlock.actionId = pugi::cast<int32_t>(attr.value());
	}

	if ((attr = node.attribute("text"))) {
		lootBlock.text = attr.as_string();
	}
	return true;
}

PokeType_t Monsters::getPokeType(const pugi::xml_attribute& attr)
{
	std::string tmpStrValue = asLowerCaseString(attr.as_string());
	uint16_t tmpInt = pugi::cast<uint16_t>(attr.value());
	if (tmpStrValue == "bug" || tmpInt == 1) {
		return TYPE_BUG;
	} else if (tmpStrValue == "dragon" || tmpInt == 2) {
		return TYPE_DRAGON;
	} else if (tmpStrValue == "fairy" || tmpInt == 3) {
		return TYPE_FAIRY;
	} else if (tmpStrValue == "fire" || tmpInt == 4) {
		return TYPE_FIRE;
	} else if (tmpStrValue == "ghost" || tmpInt == 5) {
		return TYPE_GHOST;
	} else if (tmpStrValue == "ground" || tmpInt == 6) {
		return TYPE_GROUND;
	} else if (tmpStrValue == "normal" || tmpInt == 7) {
		return TYPE_NORMAL;
	} else if (tmpStrValue == "psychic" || tmpInt == 8) {
		return TYPE_PSYCHIC;
	} else if (tmpStrValue == "steel" || tmpInt == 9) {
		return TYPE_STEEL;
	} else if (tmpStrValue == "dark" || tmpInt == 10) {
		return TYPE_DARK;
	} else if (tmpStrValue == "electric" || tmpInt == 11) {
		return TYPE_ELECTRIC;
	} else if (tmpStrValue == "fighting" || tmpInt == 12) {
		return TYPE_FIGHTING;
	} else if (tmpStrValue == "flying" || tmpInt == 13) {
		return TYPE_FLYING;
	} else if (tmpStrValue == "grass" || tmpInt == 14) {
		return TYPE_GRASS;
	} else if (tmpStrValue == "ice" || tmpInt == 15) {
		return TYPE_ICE;
	} else if (tmpStrValue == "poison" || tmpInt == 16) {
		return TYPE_POISON;
	} else if (tmpStrValue == "rock" || tmpInt == 17) {
		return TYPE_ROCK;
	} else if (tmpStrValue == "water" || tmpInt == 18) {
		return TYPE_WATER;
	}
	else {
		return TYPE_NONE;
	}
}

void Monsters::loadLootContainer(const pugi::xml_node& node, LootBlock& lBlock)
{
	for (auto subNode : node.children()) {
		LootBlock lootBlock;
		if (loadLootItem(subNode, lootBlock)) {
			lBlock.childLoot.emplace_back(std::move(lootBlock));
		}
	}
}

MonsterType* Monsters::getMonsterType(const std::string& name)
{
	auto it = monsters.find(asLowerCaseString(name));

	if (it == monsters.end()) {
		return nullptr;
	}
	return &it->second;
}
