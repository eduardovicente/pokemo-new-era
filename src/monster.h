/**
 * The Forgotten Server - a free and open-source MMORPG server emulator
 * Copyright (C) 2016  Mark Samman <mark.samman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef FS_MONSTER_H_9F5EEFE64314418CA7DA41D1B9409DD0
#define FS_MONSTER_H_9F5EEFE64314418CA7DA41D1B9409DD0

#include "tile.h"
#include "monsters.h"

class Creature;
class Game;
class Spawn;

typedef std::unordered_set<Creature*> CreatureHashSet;
typedef std::list<Creature*> CreatureList;

enum TargetSearchType_t {
	TARGETSEARCH_DEFAULT,
	TARGETSEARCH_RANDOM,
	TARGETSEARCH_ATTACKRANGE,
	TARGETSEARCH_NEAREST,
};

struct MonsterSpell {
	MonsterSpell() {};
	MonsterSpell(const spellBlock_t& spell) {
		spellBlock = &spell;
	}

	const spellBlock_t* spellBlock = nullptr;
	int64_t lastCooldown = 0;
};

class Monster final : public Creature
{
	public:
		static Monster* createMonster(const std::string& name);
		static int32_t despawnRange;
		static int32_t despawnRadius;

		explicit Monster(MonsterType* mtype);
		~Monster();

		// non-copyable
		Monster(const Monster&) = delete;
		Monster& operator=(const Monster&) = delete;

		Monster* getMonster() final {
			return this;
		}
		const Monster* getMonster() const final {
			return this;
		}

		void setID() final {
			if (id == 0) {
				id = monsterAutoID++;
			}
		}

		void removeList() final;
		void addList() final;

		const std::string& getName() const final {
			return mType->name;
		}
		const std::string& getNameDescription() const final {
			return mType->nameDescription;
		}
		std::string getDescription(int32_t) const final {
			return strDescription + '.';
		}

		CreatureType_t getType() const final {
			return CREATURETYPE_MONSTER;
		}

		const Position& getMasterPos() const {
			return masterPos;
		}
		void setMasterPos(Position pos) {
			masterPos = pos;
		}

		bool isPushable() const final {
			return mType->pushable && baseSpeed != 0;
		}
		bool isAttackable() const final {
			return mType->isAttackable;
		}

		bool canPushItems() const {
			return mType->canPushItems;
		}
		bool canPushCreatures() const {
			return mType->canPushCreatures;
		}
		bool isHostile() const {
			return mType->isHostile;
		}
		bool canSee(const Position& pos) const final;
		bool canSeeInvisibility() const final {
			return isImmune(CONDITION_INVISIBLE);
		}

		void setSpawn(Spawn* spawn) {
			this->spawn = spawn;
		}

		void castSpell(std::string word);

		void onAttackedCreatureDisappear(bool isLogout) final;

		void onCreatureAppear(Creature* creature, bool isLogin) final;
		void onRemoveCreature(Creature* creature, bool isLogout) final;
		void onCreatureMove(Creature* creature, const Tile* newTile, const Position& newPos, const Tile* oldTile, const Position& oldPos, bool teleport) final;
		void onCreatureSay(Creature* creature, SpeakClasses type, const std::string& text) final;

		void drainHealth(Creature* attacker, int32_t damage) final;
		void changeHealth(int32_t healthChange, bool sendHealthChange = true) final;
		void onWalk() final;
		bool getNextStep(Direction& direction, uint32_t& flags) final;
		void onFollowCreatureComplete(const Creature* creature) final;

		void onThink(uint32_t interval) final;

		bool challengeCreature(Creature* creature) final;
		bool convinceCreature(Creature* creature) final;

		void setNormalCreatureLight() final;

		void doAttacking(uint32_t interval);
		bool tryToUseSpell(const Position& myPos, const Position& targetPos, MonsterSpell& spell);

		bool searchTarget(TargetSearchType_t searchType = TARGETSEARCH_DEFAULT);
		bool selectTarget(Creature* creature);

		const CreatureList& getTargetList() const {
			return targetList;
		}
		const CreatureHashSet& getFriendList() const {
			return friendList;
		}

		bool isTarget(const Creature* creature) const;
		bool isFleeing() const {
			return getHealth() <= mType->runAwayHealth;
		}

		bool getDistanceStep(const Position& targetPos, Direction& direction, bool flee = false);
		bool isTargetNearby() const {
			return stepDuration >= 1;
		}

		BlockType_t blockHit(Creature* attacker, CombatType_t combatType, int32_t& damage,
		                     bool checkDefense = false, bool checkArmor = false, bool field = false, CombatSpellType_t spellType = COMBATSPELL_NONE);

		static uint32_t monsterAutoID;

		uint64_t getExperience();
		void setExperience(uint64_t exp);

		static uint64_t getExpForLevel(int32_t lv);
		static uint8_t getPercentLevel(uint64_t count, uint64_t nextLevelCount);

		void addExperience(Creature* source, uint64_t exp, bool sendText = false);
		void gainExperience(Creature* source, uint64_t gainExp);
		void onGainExperience(uint64_t gainExp, Creature* target) final;

		uint64_t getGainedExperience(Creature* attacker) const final;
		double getLostPercent() const;
		uint64_t getLostExperience() const final {
			return skillLoss ? static_cast<uint64_t>(experience * getLostPercent()) : 0;
		}


		const uint32_t getLevel() const;
        void setLevel(uint32_t lvl) { level = lvl; }
		void onAdvancedLevel();

		void updateSkills();

		void updateMaxHealth();

		uint32_t getAttack();
		void updateAttack();

		uint32_t getDefense();
		void updateDefense();

		uint32_t getSpecialAttack();
		void updateSpecialAttack();

		uint32_t getSpecialDefense();	
		void updateSpecialDefense();

		void updateSpeed();

		double getBaseStatus(MonsterStatus_t statusType);
		void setBaseStatus();

		double getStatus(MonsterStatus_t statusType);
		void setStatus(MonsterStatus_t statusType, double value);
		void addStatus(MonsterStatus_t statusType, double value);

		double getExtraStatus(MonsterStatus_t statusType);
		void setExtraStatus(MonsterStatus_t statusType, double value);
		void addExtraStatus(MonsterStatus_t statusType, double value);

		uint32_t getAttributesPoints();
		void setAttributesPoints(uint32_t value);
		void addAttributesPoints(uint32_t value);

		void setControlled(bool controlled);
		bool getIsControlled();

		bool hasAbility(AbilityType_t ability);
	private:		
		MonsterType* mType;
		std::string strDescription;		

		Spawn* spawn;

		uint32_t level = 0;
		uint32_t attributesPoints = 0;

		status_t status;
		status_t extraStatus;
		
		int64_t lastMeleeAttack;
		MonsterSpell normalAttack;
		std::vector<MonsterSpell> attackSpells;
		std::vector<MonsterSpell> passiveSpells;

		uint64_t experience = 0;
		uint8_t levelPercent = 0;

		CreatureHashSet friendList;
		CreatureList targetList;		

		uint32_t spellTicks;
		uint32_t targetTicks;
		
		uint32_t yellTicks;
		uint32_t passiveTicks;
		uint32_t summonTicks;

		uint32_t targetChangeTicks;
		int32_t targetChangeCooldown;

		int32_t stepDuration;

		Position masterPos;

		double attack = 0;
		double defense = 0;
		double specialAttack = 0;
		double specialDefense = 0;

		bool isIdle;
		bool isMasterInRange;

		void onCreatureEnter(Creature* creature);
		void onCreatureLeave(Creature* creature);
		void onCreatureFound(Creature* creature, bool pushFront = false);

		void updateLookDirection();

		void addFriend(Creature* creature);
		void removeFriend(Creature* creature);
		void addTarget(Creature* creature, bool pushFront = false);
		void removeTarget(Creature* creature);

		void updateTargetList();
		void clearTargetList();
		void clearFriendList();

		void death(Creature* lastHitCreature) final;
		Item* getCorpse(Creature* lastHitCreature, Creature* mostDamageCreature) final;

		void setIdle(bool idle);
		void updateIdleStatus();
		bool getIdleStatus() const {
			return isIdle;
		}

		void onAddCondition(ConditionType_t type) final;
		void onEndCondition(ConditionType_t type) final;
		void onCreatureConvinced(const Creature* convincer, const Creature* creature) final;

		bool canUseAttack(const Position& pos, const Creature* target) const;
		bool canUseSpell(const Position& pos, const Position& targetPos,const MonsterSpell& spell, bool& inRange);
		bool getRandomStep(const Position& creaturePos, Direction& direction) const;
		bool getDanceStep(const Position& creaturePos, Direction& direction, bool keepAttack = true, bool keepDistance = true);
		bool isInSpawnRange(const Position& pos) const;
		bool canWalkTo(Position pos, Direction direction) const;

		static bool pushItem(Item* item);
		static void pushItems(Tile* tile);
		static bool pushCreature(Creature* creature);
		static void pushCreatures(Tile* tile);

		void onThinkTarget(uint32_t interval);
		void onThinkYell(uint32_t interval);
		void onThinkPassive();
		void onThinkSummons(uint32_t interval);

		bool isFriend(const Creature* creature) const;
		bool isOpponent(const Creature* creature) const;

		uint16_t getLookCorpse() const final {
			return mType->lookcorpse;
		}
		void dropLoot(Container* corpse, Creature* lastHitCreature) final;
		uint64_t getDamageImmunities() const final {
			return mType->damageImmunities;
		}
		uint32_t getConditionImmunities() const final {
			return mType->conditionImmunities;
		}
		void getPathSearchParams(const Creature* creature, FindPathParams& fpp) const final;
		bool useCacheMap() const final {
			return true;
		}

		friend class LuaScriptInterface;
};

#endif
